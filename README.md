﻿# 微信小程序
## 微信小程序源码：美容美发小程序。

#### 项目介绍

	此项目是一套可用于在小程序客户端上用户可以根据系统设置的时间段进行预约美容美发的系统

#### 功能介绍

	1. 可在后台编辑添加技师信息列表
	2. 可在后台编辑添服务类型
	3. 后台可配置不同技师归类不同的技师
	4. 后台可配置服务类型的日期时间段、价格等的数据
	4. 后台还可售卖商品
	5. 其他基本功能

    - Tip 更换页面，在app.json里面设置路径即可
	
### QQ交流群 — 24934459
### 公司官网 - http://www.fz33.net  官网
### 管理后台效果图（部分图）

|登录入口：http://www.sansancloud.com/manager/#/login|试用账号：yanshi 密码：yanshi123
|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_63.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_885.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_956.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_873.jpg)
### 效果图---扫码查看（部分图片）

|往下扫码预览|往下扫码预览|往下扫码预览|
|:----:|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_294.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_152.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_201.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_336.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_339.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/20_139.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_594.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_601.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/53/19_569.jpg)
### 公司资质

|省高薪证书|国高薪证书|
|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/8_826.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/6_933.jpg)
    

![file-list](http://image1.sansancloud.com/xianhua/2021_3/30/12/55/40_320.jpg)