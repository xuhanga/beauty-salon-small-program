const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    display:"none"
  },
  ready:function(){
    let that=this;
    app.globalData.consoleFun("image_link组件-数据",[that.data.data])
    if (that.data.data.androidTemplate == "popimage"){
      try {
        var imageUrl = wx.getStorageSync('popimage_' + that.data.data.id)
        // 当缓存的图片地址存在以及他的图片不变的时候不需要弹窗
        app.globalData.consoleFun("imageUrl",[imageUrl, that.data.data.jsonData.imageUrl, imageUrl == that.data.data.jsonData.imageUrl])
        if (imageUrl && imageUrl == that.data.data.jsonData.imageUrl) {
          app.globalData.consoleFun("存在imageUrl",[imageUrl])
          that.setData({
            display: "none"
          })
        }
        // 其余的时候显示弹窗
        else {
          app.globalData.consoleFun("不存在imageUrl",[imageUrl])
          setTimeout(function () {
            that.setData({
              display: "block"
            })
          }, 1000)
        }
      } catch (e) { }
    }
  },
  methods: {
    // 这里是一个自定义方法
    saveImageToLocal: function (e) {
      let imgSrc = e.currentTarget.dataset.imageurl
      app.globalData.consoleFun("",[imgSrc])
      let PostImageSrc = imgSrc.replace(/http/, "https")
      app.globalData.consoleFun("",[PostImageSrc])
      if (!imgSrc) {
        return
      }
      let urls = []
      urls.push(imgSrc)
      wx.previewImage({
        current: imgSrc, // 当前显示图片的http链接
        urls: urls // 需要预览的图片http链接列表
      })
    },
    tolinkUrl: function (event) {
      let that=this;
      app.globalData.consoleFun("",[event.currentTarget.dataset.link])
      app.globalData.consoleFun("======s=======",[event.currentTarget.dataset.url])
      // 缓存
      app.globalData.consoleFun("======popimage=======",[that.data.data.androidTemplate])
      let state=app.globalData.linkEvent(event.currentTarget.dataset.link,"",function(){
          app.globalData.consoleFun("======s=======",)
          that.showPoster()
        });
      app.globalData.consoleFun("======state=======",[state])
      if (state !='authorization'){
        if (that.data.data.androidTemplate == "popimage") {
          try {
            wx.setStorageSync('popimage_' + that.data.data.id, event.currentTarget.dataset.url)
          } catch (e) { }
          that.setData({
            display: 'none'
          })
        }
      }
    },
    showPoster: function () {
      let that = this;
      app.globalData.consoleFun("===showPoster====", [app.globalData.loginUser.id])
      if (app.globalData.loginUser && app.globalData.loginUser.platformUser.id) {
        let ewmImgUrl = app.globalData.getQrCode({ type: "user_info", id: app.globalData.loginUser.platformUser.id })
        that.setData({
          posterState: true,
          ewmImgUrl: ewmImgUrl,
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '您还未登录,请您登录后在操作~',
          success: function (res) {
            if (res.confirm) {
              app.globalData.linkEvent('app_login_page.html');
            } else if (res.cancel) {

            }
          }
        })
      }
    },
    getChilrenPoster:function(){
      let that=this;
      that.setData({
        posterState: false,
      })
    },
    closeFun:function(e){
      app.globalData.consoleFun("===========e==========", [e.currentTarget.dataset.url])
      // 缓存
      try {
        wx.setStorageSync('popimage_' + this.data.data.id,e.currentTarget.dataset.url)
      } catch (e) {
      }
      this.setData({
        display:'none'
      })
    }
  },
})