const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    display:"block"
  },
  ready:function(){
    console.log("imgPopup数据", this.data.data)
    let that=this;
    this.setData({
      display: 'block',
      someData: this.data.data
    })
  },
  methods: {
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      console.log("===========e==========", event.currentTarget.dataset.url)
      try {
        wx.setStorageSync('popimage', event.currentTarget.dataset.url)
      } catch (e) {
      }
      this.setData({
        display: 'none'
      })
      app.globalData.linkEvent(event.currentTarget.dataset.link);

    },
    closeFun:function(e){
      console.log("===========e==========", e.currentTarget.dataset.url)
      try {
        wx.setStorageSync('popimage',e.currentTarget.dataset.url)
      } catch (e) {
      }
      this.setData({
        display:'none'
      })
    }
  },
})