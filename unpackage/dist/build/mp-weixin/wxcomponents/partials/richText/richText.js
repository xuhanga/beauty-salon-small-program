const app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {}
  },
  ready:function(){
    var that = this
    var oldData=that.data.receiveData;
    app.globalData.consoleFun("=====gridist组件-richText=====",[oldData])
    WxParse.wxParse('article', 'html', oldData.jsonData.content, that, 10);
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})